import { Component, OnInit } from '@angular/core';
import { Book } from './book';
import { BooksService } from '../books.service';
import { AddToCartService } from '../add-to-cart.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {

  cartSize = 0;
  books: Book[] = [];
  searchBook: string;
 

  constructor(private booksService: BooksService, private addtoCartService: AddToCartService) {

  }

  ngOnInit() {
    this.booksService.getBooks()
      .subscribe((data: Book[]) => {
        this.books = data;
      })
  }


  addToCart(book: Book) {
    this.addtoCartService.addToCart(book);
  }

}
