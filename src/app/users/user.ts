export interface User {
    
    userId: number;
    firstName: string;
    middleName?: string;
    lastName: string;
    userName: string;
    email: string;
    contact: string;
}
