import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private userServiceUrl= 'http://localhost:8097/lib/v2/users';
   
  constructor(private http: HttpClient) {}

  getUsers() {
    return this.http.get(this.userServiceUrl);
  }
}
