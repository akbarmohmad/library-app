import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AddToCartService } from 'src/app/add-to-cart.service';
import { Book } from '../book';


@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.scss']
})
export class BookDetailsComponent implements OnInit {


  @Input('bookDetail') book: Book;

  @Output() addToCart = new EventEmitter<Book>();

  constructor() { }

  ngOnInit() {
  }

  addBookToCart(book: Book) {
    this.addToCart.emit(book);

  }
}
