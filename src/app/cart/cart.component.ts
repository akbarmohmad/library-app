import { Component, OnInit } from '@angular/core';
import { AddToCartService } from '../add-to-cart.service';
import { Book } from '../books/book';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  cartSize = 0; 
  products : Book[] = [];
  constructor(private addToCartService: AddToCartService) {
    
  }

  ngOnInit() {
    this.addToCartService.addToCart$
                                    .subscribe(book=>{
                                      console.log(book);
                                     this.products.push(book);
                                     this.cartSize++;
                                    });
  }

}
