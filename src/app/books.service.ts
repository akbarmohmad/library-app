import { Injectable } from '@angular/core';
import { Book } from './books/book';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class BooksService {
  books = [];
  private bookServiceUrl = 'assets/data/books.json';
  //private bookServiceUrl= 'http://localhost:8097/lib/v2/books';

  constructor(private http: HttpClient) {
  }

  getBooks() {
    // return of(this.books);
    return this.http.get(this.bookServiceUrl);
  }

  addBook(book: Book) {
    return this.http.post(this.bookServiceUrl, book);
  }

}
