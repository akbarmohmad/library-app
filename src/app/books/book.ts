export interface Book {

     bookId: number;
     title: string;
     author: string;
     price: string;
     publishedYear: string;
     quantity : number;

}