import { Injectable } from '@angular/core';
import { Book } from './books/book';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddToCartService {


  private addToCartSource = new Subject<Book>();
//expose as observable
  addToCart$ = this.addToCartSource.asObservable();
  constructor() { }


  addToCart(book : Book){
    console.log("AddToCartService called")
    this.addToCartSource.next(book);
  }
}
