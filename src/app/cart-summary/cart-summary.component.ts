import { Component, OnInit, ViewChild, AfterViewInit, AfterContentChecked } from '@angular/core';
import { CartComponent } from '../cart/cart.component';
import { Book } from '../books/book';

@Component({
  selector: 'app-cart-summary',
  templateUrl: './cart-summary.component.html',
  styleUrls: ['./cart-summary.component.scss']
})
export class CartSummaryComponent implements OnInit, AfterContentChecked {

  books: Book[] = [];

  total: number = 0;


  ngAfterContentChecked() {
    this.total = this.books.reduce((cur, book) => {
      cur = cur + (parseInt(book.price)*book.quantity);
      return cur;
    }, 0);
  }

  private cartComponent: CartComponent;
  constructor() { }

  ngOnInit() {
    let cartBooks = history.state.data || [];
    let bookQtyByName = cartBooks.reduce((acc, it) => {
      acc[it.title] = acc[it.title] + 1 || 1;
      return acc;
    }, {});
    console.log(bookQtyByName);
    let uniqueBooks = [...new Set<Book>(cartBooks)];
    console.log(uniqueBooks);
    for (let book of uniqueBooks) {
      book.quantity = bookQtyByName[book.title];
      console.log(book.quantity);
    }
    this.books = uniqueBooks;
    // this.books = history.state.data;
  }

}
