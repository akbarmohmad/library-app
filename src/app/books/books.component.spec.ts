import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BooksComponent } from './books.component';
import { BooksService } from '../books.service';
import { HttpClient } from '@angular/common/http';
import { UsersService } from '../users.service';
import { Book } from './book';
import { Observable } from 'rxjs';

describe('BooksComponent', () => {
  let component: BooksComponent;
  let fixture: ComponentFixture<BooksComponent>;
  let booksService: BooksService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BooksComponent ],
      providers: [{provide: BooksService, useClass: MockBookService}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BooksComponent);
    component = fixture.componentInstance;
    booksService = TestBed.get(BooksService)
    fixture.detectChanges();
  });

  it('should create', () => {
    component.ngOnInit();
    expect(component.books).toBe(BOOKS_OBJECT);
  });
});

const BOOKS_OBJECT: Book[] =[
  {"bookId": 1, "author" : "Akbar1" , "title": "MyBook1", "price": "50", "publishedYear":"2018"}];

class MockBookService {
  public getBooks(): Observable<Book> {
    return Observable.of(BOOKS_OBJECT);
  }
  
};
