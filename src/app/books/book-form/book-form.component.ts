import { Component, OnInit } from '@angular/core';
import { Book } from '../book';
import { BooksService } from 'src/app/books.service';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss']
})
export class BookFormComponent implements OnInit {

  constructor(private booksService: BooksService) { }

  ngOnInit() {
  }

  submitted = false;

  onSubmit(bookFormData: Book) {
     this.submitted = true; 
    this.booksService.addBook(bookFormData).subscribe();
     }

 
}
