import { Component, OnInit } from '@angular/core';
import { Book } from './books/book';
import { AddToCartService } from './add-to-cart.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Library';

  cartSize = 0; 
  cartItems : Book[] = [];
  constructor(private addToCartService: AddToCartService) {
    
  }

  ngOnInit() {
    this.addToCartService.addToCart$
                                    .subscribe(book=>{
                                      console.log(book);
                                     this.cartItems.push(book);
                                     this.cartSize++;
                                    });
  }

}
