import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { BookFormComponent } from './books/book-form/book-form.component';
import { BooksComponent } from './books/books.component';
import { CartSummaryComponent } from './cart-summary/cart-summary.component';
import { HomeComponent } from './home/home.component';
import { UserFormComponent } from './users/user-form/user-form.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {path:'', redirectTo: 'home', pathMatch:'full'},
  {path: 'home', component: HomeComponent},
  {path: 'books', component: BooksComponent},
  // {path: 'addBook', component: BookFormComponent},
  {path: 'users', component: UsersComponent},
   {path: 'cartSummary', component: CartSummaryComponent},
  {path:'admin', component: AdminComponent,children:[
    {path: 'addBook', component: BookFormComponent},
    {path: 'addUser', component: UserFormComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
